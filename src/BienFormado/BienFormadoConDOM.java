package BienFormado;

import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class BienFormadoConDOM{

    public static void main(String[] args) {
        String rutaXML = "src/Recursos/Alumno.xml";

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setValidating(true);
            DocumentBuilder builder = factory.newDocumentBuilder();
            builder.setErrorHandler(new SimpleErrorHandler());
            builder.parse(new InputSource(rutaXML));
        } catch (ParserConfigurationException e){           
        } catch (SAXException e){        
        } catch (IOException e) {        
        }
    }
}