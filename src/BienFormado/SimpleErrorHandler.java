/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BienFormado;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 *
 * @author diurno2020
 */
public class SimpleErrorHandler implements ErrorHandler {

    public SimpleErrorHandler() {
    }

    @Override
    public void warning(SAXParseException exception) throws SAXException {
        System.out.println("Warning");
    }

    @Override
    public void error(SAXParseException exception) throws SAXException {
       System.out.println("Error");
    }

    @Override
    public void fatalError(SAXParseException exception) throws SAXException {
        System.out.println("FatalError");
    }
    
}
