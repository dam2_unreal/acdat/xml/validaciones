package BienFormado;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class BienFormadoConSAX {
     public static void main(String[] args) {
        String rutaXML = "src/Recursos/Alumno.xml";

        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            factory.setValidating(false);
            
            SAXParser parser = factory.newSAXParser();
            XMLReader reader = parser.getXMLReader();
            
            reader.setErrorHandler(new SimpleErrorHandler());
            reader.parse(new InputSource(rutaXML));
        } catch (ParserConfigurationException e){           
        } catch (SAXException e){        
        } catch (IOException e) {        
        }
    }
}
